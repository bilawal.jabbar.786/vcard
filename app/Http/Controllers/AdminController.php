<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Vcf;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Response;
class AdminController extends Controller
{
    public function clientcreate(){
        return view('admin.client.create');
    }
    public function vcfcreate(){
        return view('admin.vcf.create');
    }
    public function clientindex(){
        $clients = Clients::all();
        return view('admin.client.clientindex', compact('clients'));
    }
    public function vcfindex(){
        $vcf = Vcf::all();
        return view('admin.vcf.index', compact('vcf'));
    }
    public function clientstore(Request $request){
        $client = new Clients();
        $client->name = $request->name;
        $client->email = $request->email;
        $client->phone = $request->phone;
        $client->designation = $request->designation;
        $client->qrocde = '1';
        $client->cardimage = '1';

        $client->link = Str::slug($request->name) . '-' . rand(1000000, 9000000);

       /* if ($request->hasfile('photo')) {
            $image1 = $request->file('photo');
            $name = time() . 'photo' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'photo/';
            $image1->move($destinationPath, $name);
            $client->cardimage = 'photo/' . $name;
        }*/
       /* if ($request->hasfile('qrocde')) {
            $image1 = $request->file('qrocde');
            $name = time() . 'qrocde' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'qrocde/';
            $image1->move($destinationPath, $name);
            $client->qrocde = 'qrocde/' . $name;
        }*/
        if ($request->hasfile('qrocde2')) {
            $image1 = $request->file('qrocde2');
            $name = time() . 'qrocde2' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'qrocde2/';
            $image1->move($destinationPath, $name);
            $client->qrocde2 = 'qrocde2/' . $name;
        }
        if ($request->hasfile('pdf')) {
            $image1 = $request->file('pdf');
            $name = time() . 'pdf' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'pdf/';
            $image1->move($destinationPath, $name);
            $client->pdf = 'pdf/' . $name;
        }
        if ($request->hasfile('icon')) {
            $image1 = $request->file('icon');
            $name = time() . 'icon' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'icon/';
            $image1->move($destinationPath, $name);
            $client->icon = 'icon/' . $name;
        }
        $client->save();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function vcfstore(Request $request){
        $vcf = new Vcf();
        if ($request->hasfile('vcf')) {
            $image1 = $request->file('vcf');
            $name = time() . 'vcf' . '.' . $image1->getClientOriginalExtension();
            $destinationPath = 'vcf/';
            $image1->move($destinationPath, $name);
            $vcf->vcf = 'vcf/' . $name;
        }
        $vcf->link = 'Vcf' . '-' . rand(1000000, 9000000);
        $vcf->name = $request->name;
        $vcf->save();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function changestatus($status, $id){
        $client = Clients::find($id);
        $client->status = $status;
        $client->update();
        $notification = array(
            'messege' => 'Sauvegarde réussie!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function clientdelete($id){
        $client = Clients::find($id);
        $client->delete();
        $notification = array(
            'messege' => 'Effacer!',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }
    public function vcfdelete($id){
        $client = Vcf::find($id);
        $client->delete();
        $notification = array(
            'messege' => 'Effacer!',
            'alert-type' => 'error'
        );
        return redirect()->back()->with($notification);
    }
    public function vcfdownload($id){
        $vcf = Vcf::find($id);
        $file= public_path().'/'. $vcf->vcf;

        $headers = array(
            'Content-Type: application/vcf',
        );

        return Response::download($file, $vcf->name.'.vcf', $headers);
    }
}
