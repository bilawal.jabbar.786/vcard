<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Vcf;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index($slug){
        $client = Clients::where('link', $slug)->where('status', '0')->first();
        if ($client){
            return view('front.card', compact('client'));
        }else{
            abort(404);
        }
    }
    public function vcf($slug){
        $vcf = Vcf::where('link', $slug)->first();
        if ($vcf){
            return view('front.vcf', compact('vcf'));
        }else{
            abort(404);
        }
    }
}
