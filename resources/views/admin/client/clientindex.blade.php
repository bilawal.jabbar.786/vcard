@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!-- <h1>DataTables</h1> -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Domicile</a></li>
                            <li class="breadcrumb-item active">Tous les Clients</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Tous les Clients</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>E-mail</th>
                                        <th>Photo</th>
                                        <th>Telephone</th>
                                        <th>Lien unique</th>
                                        <th>Copy Lien</th>
                                        <th>Status</th>
                                        <th>Effacer</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($clients as $row)
                                        <tr>
                                            <td>{{$row->name}}</td>
                                            <td>{{$row->email}}</td>
                                            <td>
                                                <img src="{{asset($row->cardimage)}}" height="50px" alt="">
                                            </td>
                                            <td>{{$row->phone}}</td>
                                            <td id="copycontent">{{URL::to('/').'/card/'.$row->link}}</td>
                                            <td>
                                                <a onclick="mycopyFunction()" class="btn btn-sm btn-danger">
                                                  Copy
                                                </a>
                                            </td>
                                            <td>
                                                @if($row->status == '1')
                                                    <a href="{{route('change.status', ['status' => '0', 'id' => $row->id])}}"><button class="btn btn-sm btn-primary">Activate</button></a>
                                                @else
                                                    <a href="{{route('change.status', ['status' => '1', 'id' => $row->id])}}"><button class="btn btn-sm btn-danger">Deactivate</button></a>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{route('client.delete', ['id' => $row->id])}}" id="delete" class="btn btn-sm btn-danger" data-toggle="tooltip" title="edit">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <script>
        function mycopyFunction() {
            var copyText = document.getElementById("copycontent").innerText;
            var elem = document.createElement("textarea");
            document.body.appendChild(elem);
            elem.value = copyText;
            elem.select();
            document.execCommand("copy");
            document.body.removeChild(elem);
            $(document).Toasts('create', {
                class: 'bg-success',
                title: 'Succès',
                body: 'Copie du lien réussie'
            })
        }
    </script>
@endsection
