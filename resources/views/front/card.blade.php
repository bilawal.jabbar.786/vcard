<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{$client->name}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <link rel="shortcut icon" href="{{asset($client->icon)}}">
    <link rel="shortcut icon" href="{{asset($client->icon)}}">

    <link rel="apple-touch-icon" href="{{asset($client->icon)}}">
{{--    <link rel="manifest" href="{{asset('manifest.json')}}">--}}

{{--    <link rel="manifest" href="{{asset('manifest.json')}}">--}}


</head>
<style>
    .button {
        background-color: black;
        border: none;
        color: white;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
        width: 100%;
    }
    .add-button {
        position: absolute;
        top: 1px;
        left: 1px;
    }
    @media only screen and (max-width: 600px) {
       .p0{
           padding: 0px !important;
       }
    }
    .demo-iframe-holder {
        width: 100%;
        height: 680px;
        -webkit-overflow-scrolling: touch;
        overflow-y: scroll;
    }

    .demo-iframe-holder iframe {
        height: 100%;
        width: 100%;
    }
</style>
<body>
{{--<button class="add-button">Add to home screen</button>--}}

<div class="container-fluid" style="text-align: center; padding: 40px; font-family: Arial, Helvetica, sans-serif; background-color: black;  color: white">
    <h1 >{{$client->name}}</h1>
    <p >{{$client->designation}}</p>
</div>

<div class="container p0" style="padding: 40px">
    <div class="row">
        <div class="col-md-4" style="text-align: center">
            <h1>    <b>CARTE DE VISITE</b> </h1><br>
            {!! QrCode::size(330)->generate(asset($client->pdf)); !!}
{{--            <img src="{{asset($client->qrocde)}}" style="width: 100%" alt="">--}}
            <button class="button">SCANNE MOI</button>
        </div>

        <div class="col-md-4">
{{--            <iframe src="{{asset($client->pdf)}}" height="500" width="100%" title="Iframe Example"></iframe>--}}
            <div class="demo-iframe-holder">
                <iframe  src="https://docs.google.com/gview?url={{asset($client->pdf)}}&embedded=true"  ></iframe>
            </div>

{{--            <img src="{{asset($client->cardimage)}}" style="width: 100%" alt="">--}}
            <a href="{{asset($client->pdf)}}">
                <button class="button">CARTE DE VISITE PDF</button>
            </a>
        </div>
        <div class="col-md-4" style="text-align: center">
            <h1>    <b>CARTE VCF</b> </h1><br>
            <img src="{{asset($client->qrocde2)}}" style="width: 100%" alt="">
            <button class="button">SCANNE MOI</button>
{{--            <a onclick="MakeShortcut()">clik</a>--}}

        </div>
    </div>
</div>
<script>
    if ('serviceWorker' in navigator) {
        console.log("Will the service worker register?");
        navigator.serviceWorker.register('service-worker.js')
            .then(function(reg){
                console.log("Yes, it did.");
            }).catch(function(err) {
            console.log("No it didn't. This happened:", err)
        });
    }
</script>
<script language="Javascript">
    function setBookmark(url,str){
        if(str=='')str=url;
        if (document.all)window.external.AddFavorite(url,str);
        else alert('Press CTRL and D to add a bookmark to:\n"'+url+'".');
    }
</script>

<script>
    function MakeShortcut() {
        var WshShell = new ActiveXObject("WScript.Shell");
        strDesktop = WshShell.SpecialFolders("Desktop");
        var oUrlLink = WshShell.CreateShortcut(strDesktop + "\Link to Help Page.url");
        oUrlLink.TargetPath = "http://www.someurl.com";
        oUrlLink.Save();
    }
</script>
</body>
</html>
