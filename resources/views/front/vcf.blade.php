<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{$vcf->name}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <style>
        .button {
            background-color: black;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            width: 100%;
        }
    </style>
</head>
<body>

<div class="container" style="padding-top: 300px">
    <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-4">
            <a href="{{route('vcf.download', ['id' => $vcf->id])}}"> <button class="button">Télécharger maintenant</button></a>

        </div>
        <div class="col-md-4">

        </div>
    </div>
</div>

</body>
</html>
