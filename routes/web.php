<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('qr-code-g', function () {

    \QrCode::size(500)
        ->format('png')
        ->generate('ItSolutionStuff.com', public_path('assets/01.png'));

    return view('qrCode');

});
Route::get('/', function () {
    return view('auth.login');
});
Route::get('/card/{slug}','FrontendController@index')->name('front.vcard');
Route::get('/vcf/{slug}','FrontendController@vcf')->name('front.vcf');
Route::get('/vdf/download/{id}', 'AdminController@vcfdownload')->name('vcf.download');

Auth::routes();

Route::group(['middleware' => ['auth', 'web']], function() {

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/change/status/{status}/{id}', 'AdminController@changestatus')->name('change.status');

Route::get('/client/index', 'AdminController@clientindex')->name('client.index');
Route::get('/client/create', 'AdminController@clientcreate')->name('client.create');
Route::post('/client/store', 'AdminController@clientstore')->name('client.store');
Route::get('/client/delete/{id}', 'AdminController@clientdelete')->name('client.delete');

Route::get('/admin/vcf/index', 'AdminController@vcfindex')->name('vcf.index');
Route::get('/admin/vcf/create', 'AdminController@vcfcreate')->name('vcf.create');
Route::post('/admin/vcf/store', 'AdminController@vcfstore')->name('vcf.store');
Route::get('/admin/vcf/delete/{id}', 'AdminController@vcfdelete')->name('vcf.delete');

});
